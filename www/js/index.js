angular.module('stubbble.service', [], function($provide) {
    $provide.factory('api', ['$http', '$rootScope', function($http, $rootScope) {
        return {
            account: {
                login: function(username, password) {
                    return $http.post("/api/account/login", {
                        "username": username,
                        "password": password
                    });
                },
                logout: function() {
                    return $http.get('/api/account/logout');
                },
                get: function(userId) {
                    return $http.get('/api/user/' + userId);
                }
            },
            post: {
                create: function(post) {
                    return $http.post('/api/post/create', post);
                },
                get: function(options) {
                    var url = '/api/post/get';
                    //function addUrlParam(key) { if (options[key]) url += "/" + key + "/" + options[key] }
                    //addUrlParam('since');
                    //addUrlParam('by');
                    return $http.post(url, {options:options})
                }
            }
         }
    }]);
});

angular.module('stubbble.directive', []);
angular.module('stubbble.filter', []);
angular.module('stubbble', ['stubbble.service', 'stubbble.directive', 'stubbble.filter'], function ($locationProvider, $routeProvider) {
    $locationProvider.html5Mode(true);
    $routeProvider
        .when('/', {
            templateUrl: 'tmpl/default.html', 
            controller: RootCtrl,
            resolve: {
                user: ['api', function (api) {
                    return {
                        id: bomb.user == null ? 0 : bomb.user.id
                    }
                }]
            }
        })
        .when('/@:username', {
            templateUrl: 'tmpl/profile.html', 
            controller: ProfileCtrl,
            resolve: {
                user: ['api', function (api) {
                    return {
                        id: (bomb.user ? bomb.user.id : 0),
                        firstName: (bomb.user ? bomb.user.firstName : null),
                        lastName: (bomb.user ? bomb.user.lastName : null),
                        registerDate: (bomb.user ? bomb.user.registerDate : null)
                    }
                }],
                profile: ['api', function (api) {
                    return {
                        firstName: (bomb.profile ? bomb.profile.firstName : null),
                        lastName: (bomb.profile ? bomb.profile.lastName : null)
                    }
                }],
                posts: [ function () { return bomb.posts }]
            }
        });
});

function RootCtrl ($scope, $location, api, user) {
    $scope.user = user;
}

function NavBarCtrl ($rootScope, $scope, $location, api) {
    $scope.user = { id: bomb.user == null ? 0 : bomb.user.id, username: bomb.user == null ? null :bomb.user.name };
    $scope.login = function() {
        api.account.login($scope.username, $scope.password).then(function (response) {
            if (response.data && response.data.id > 0) {
                $scope.user.id = response.data.id;
                $rootScope.$broadcast('loggedIn', response.data);
            }
            else {
                // failed authorization
            }
            $scope.username = '';
            $scope.password = '';
        });
    }
    $scope.logout = function() {
        api.account.logout().then(function(response) {
            $scope.user.id = 0;
            $rootScope.$broadcast('loggedOut');
        });
    }
    $scope.home = function() {
        $location.path('/@' + $scope.user.username);
    }
}

function ProfileCtrl($scope, api, user, profile, posts) {
    $scope.user = user;
    $scope.profile = profile;
    $scope.posts = posts;

    $scope.post = function() {
        api.post.create({
            content: $scope.createPostText,
            authorID: $scope.user.id
        }).then(function (response) {
            alert(JSON.stringify(response.data));
        });
    };
    $scope.refresh = function() {
        var sinceDate = $scope.posts[0].createDate;
        for(i=0;i<$scope.posts.length;++i) {
            if ($scope.posts[i].createDate > sinceDate) {
                sinceDate = $scope.posts[i].createDate;
            }
        }
        api.post.get({
            since: sinceDate
        }).then(function(response) {
            alert('in the then for api.post.get:\n' + JSON.stringify(response));
        });
    };

    /// event listeners
    $scope.$on('loggedIn', function(event, _user) {
        $scope.user.id = _user.id;
    });
    $scope.$on('loggedOut', function() {
        $scope.user = { id : null };
    });

    $scope.refresh();
}

