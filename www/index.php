<?php
	// TODO: wire includes better
	require '../lib/Logger.php';
	require '../lib/Slim/Slim.php';
	require '../lib/SessionService.php';
	require '../controller/BaseController.php';
	require '../controller/DefaultController.php';
	require '../controller/ProfileController.php';
	require '../controller/ApiController.php';
	\Slim\Slim::registerAutoloader();
	
	session_start();
	$app = new \Slim\Slim(array('log.writer' => new Logger()));
	$app->add(new \Slim\Middleware\ContentTypes());

	// logging
	$logger = $app->getLog();
	$logger->setLevel(\Slim\Log::DEBUG);

	// routing
	$app->get('/', 'defaultControllerIndex');
	$app->get('/@:username', 'profileControllerIndex');
	$app->get('/tmpl/:viewName', 'renderTemplate');
	$app->map('/api/:service/:action', 'apiController')->via('GET', 'POST');
	$app->run();

	/// routing handlers
	function defaultControllerIndex() {
		$defaultController = new DefaultController(\Slim\Slim::getInstance()->request());
		$defaultController->Index();
	}
	function profileControllerIndex($username) {
		$profileController = new ProfileController(\Slim\Slim::getInstance()->request());
		$profileController->Index($username);
	}
	function apiController($service, $action) {
		$api = new ApiController(\Slim\Slim::getInstance()->request());
		$params = \Slim\Slim::getInstance()->request()->getBody();
		$api->Execute($service, $action, $params ? $params : []);
	}

	function renderTemplate($viewName) {
		include("../view/templates/".$viewName);
	}
?>