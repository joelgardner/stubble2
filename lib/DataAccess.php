<?php
define("DB_SERVER", "localhost:3306");
define("DB_USERNAME", "stubble");
define("DB_PASSWORD", "455h013");
define("DB_INITIALDB", "test");

class DataAccess 
{
    //public $logger;

	function execute_query($sqlCommand) {
		$mysqli = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_INITIALDB);

		return $mysqli->query($sqlCommand);
	}

	/// POSTS
    function get_posts($userId, $sinceId = null) {

        $sqlCommand = "call get_posts(".$userId.", null, null, null, null)";
        //$this->$logger->debug($sqlCommand);
        $result = $this->execute_query($sqlCommand);
        $posts = array();
        while($row = mysqli_fetch_assoc($result)) {
            $newRow = array();
            $newRow['content'] = $row['content'];
            $newRow['createDate'] = $row['createDate'];
            array_push($posts, $newRow);
        }

        return $posts;
    }

    function get_post($postID) {
        $sqlCmd = "call get_post(".$postID.")";
        $result = $this->execute_query($sqlCmd);
        if ($result != null) {
            $row = mysqli_fetch_assoc($result);
            if (isset($row["Error"])) {
                echo $row["Error"];
                return -1;
            }
            return $row;
        }
    }

    function create_post($userId, $postContent) {

		$mysqli = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_INITIALDB);
		$sql = $mysqli->stmt_init();
		$sql->prepare("call create_post(?, ?)");
		$sql->bind_param("is", $userId, $postContent);
		$sql->execute();
		$sql->bind_result($postID);
		$sql->fetch();
		//echo $sql->error;
		return $postID;
    }

    /// ACCOUNTS
	function add_user($email, $username, $password, $firstName, $lastName) {
		$passwordHash = $this->generate_hashed_password($password);
		$sqlCommand = "call add_user('".$email."', '".$username."', '".$passwordHash."', '".$firstName."', '".$lastName."')";
		$result = $this->execute_query($sqlCommand);
		if ($result != null) {
			$row = mysqli_fetch_assoc($result);
			if (isset($row["Error"])) {
				echo $row["Error"];
				return -1;
			}

			return $row["userID"];
		}
	}

	function user_login($username, $password) {
		$salt = $this->get_salt_by_username($username);
		if ($salt == null) {
			return false;
		}

		$passwordHash = crypt($password, $salt);
		$sqlCommand = "call user_login('".$username."', '".$passwordHash."')";
		//$logger->debug($sqlCommand);
		$result = $this->execute_query($sqlCommand);
		if ($result) {
			$row = mysqli_fetch_assoc($result);
			return $row;
		}	

		return false;
	}

	function get_salt_by_username($username) {
		$sqlCommand = "call get_salt_by_username('".$username."')";
		$result = $this->execute_query($sqlCommand);
		if ($result) {
			$row = mysqli_fetch_row($result);
			return $row[0];
		}

		echo 'no salt found.';
		return null;
	}

	function generate_hashed_password($password) {
		if (defined("CRYPT_BLOWFISH") && CRYPT_BLOWFISH) {
		 	$salt = $this->generate_random_salt();
			return crypt($password, $salt);
		}
		return null;
	}

	function generate_random_salt() {
		return '$2y$11$'.substr(md5(uniqid(rand(), true)), 0, 22);
	}

	/// PROFILES
	function get_profile($username) {
		$mysqli = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_INITIALDB);
		$sql = $mysqli->stmt_init();
		$sql->prepare("call get_profile(?)");
		$sql->bind_param("s", $username);
		$sql->execute();
		$sql->bind_result($username, $firstName, $lastName);
		$sql->fetch();
		return array (
			'username' => $username,
			'firstName' => $firstName,
			'lastName' => $lastName
		);
		//echo $username.' '.$firstName.' '.$lastName;
	}
}

?>