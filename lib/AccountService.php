<?php

class AccountService
{
	//public static $UserAccount = null;

	var $db;
	function __construct() {
		$this->db = new DataAccess();
	} 

	function login($username, $password) {
		if ($user = $this->db->user_login($username, $password)) {
			unset($_SESSION['user']);
			$_SESSION['user'] = $user;
			echo json_encode($user);
		}
		else {
			unset($_SESSION['user']);
			session_destroy();
			echo "no!";
		}
	}

	function logout() {
		unset($_SESSION['user']);
		session_destroy();
	}

	function getCurrentUser() {
		return isset($_SESSION['user']) ? $_SESSION['user'] : null;
	}
}

?>