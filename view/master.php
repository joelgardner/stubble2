<!doctype html>
<html ng-app="stubbble">
  <head>
    <script src="/js/angular.min.js"></script>
    <script src="/js/index.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/main.css" />
    <script language="javascript" type="text/javascript">
      var bomb = <?php echo json_encode($MODEL); ?>
    </script>
  </head>
  <body>
  	<?php include('layout.html') ?>
  </body>
</html>
