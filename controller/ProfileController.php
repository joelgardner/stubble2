<?php

class ProfileController extends BaseController
{
	var $db;
	function __construct($request) {
		parent::__construct($request);
		$this->db = new DataAccess();
	}
	function Index($username) {
		$accountService = new AccountService();
		$currentUser = $accountService->getCurrentUser();
		$posts = $this->db->get_posts($currentUser["id"]);
		$profile = $this->db->get_profile($username);

		$model = array(
			'posts' => $posts,
			'profile' => $profile
		);
		
		$this->Render($model);
	}
}

?>