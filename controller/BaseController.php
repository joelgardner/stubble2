<?php

class BaseController
{
	public $Request;
	function __construct($request) {
		$this->Request = $request;
	}
	function Render($MODEL) {
		$accountService = new AccountService();
		$MODEL["user"] = $accountService->getCurrentUser();
		if (isset($MODEL["user"]["passwordHash"])) unset($MODEL["user"]["passwordHash"]);
		if (isset($MODEL["user"]["lastChangeDate"])) unset($MODEL["user"]["lastChangeDate"]);
		include('../view/master.php');
	}
}

?>