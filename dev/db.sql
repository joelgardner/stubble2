SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
CREATE SCHEMA IF NOT EXISTS `test` DEFAULT CHARACTER SET latin1 ;
USE `mydb` ;
USE `test` ;

-- -----------------------------------------------------
-- Table `test`.`channel`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `test`.`channel` ;

CREATE  TABLE IF NOT EXISTS `test`.`channel` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(150) NOT NULL ,
  `desc` VARCHAR(250) NULL DEFAULT NULL ,
  `private` BIT(1) NULL DEFAULT b'0' ,
  `passwordHash` VARCHAR(50) NULL DEFAULT NULL ,
  `creatorId` INT(10) NOT NULL ,
  `createDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `lastChangeDate` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `test`.`post`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `test`.`post` ;

CREATE  TABLE IF NOT EXISTS `test`.`post` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `postTypeID` INT(10) UNSIGNED NOT NULL ,
  `authorID` INT(10) UNSIGNED NULL DEFAULT NULL ,
  `alias` VARCHAR(16) NULL DEFAULT NULL ,
  `channelID` INT(10) UNSIGNED NULL DEFAULT NULL ,
  `replyPostID` INT(10) UNSIGNED NULL DEFAULT NULL ,
  `subject` VARCHAR(256) NULL DEFAULT NULL ,
  `content` VARCHAR(4096) NOT NULL ,
  `createDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 43
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `test`.`postXtag`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `test`.`postXtag` ;

CREATE  TABLE IF NOT EXISTS `test`.`postXtag` (
  `postID` INT(10) UNSIGNED NOT NULL ,
  `tagID` INT(10) UNSIGNED NOT NULL ,
  `createDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `test`.`stubType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `test`.`stubType` ;

CREATE  TABLE IF NOT EXISTS `test`.`stubType` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(150) NOT NULL ,
  `createDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `test`.`tag`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `test`.`tag` ;

CREATE  TABLE IF NOT EXISTS `test`.`tag` (
  `id` INT(10) UNSIGNED NOT NULL ,
  `name` VARCHAR(64) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `test`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `test`.`user` ;

CREATE  TABLE IF NOT EXISTS `test`.`user` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `email` VARCHAR(150) NOT NULL ,
  `name` VARCHAR(50) NOT NULL ,
  `passwordHash` VARCHAR(50) NOT NULL ,
  `firstName` VARCHAR(45) NOT NULL ,
  `lastName` VARCHAR(45) NULL DEFAULT NULL ,
  `registerDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `lastLoginDate` DATETIME NULL DEFAULT NULL ,
  `lastChangeDate` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `ID_UNIQUE` (`id` ASC) ,
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 39
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- procedure add_user
-- -----------------------------------------------------

USE `test`;
DROP procedure IF EXISTS `test`.`add_user`;

DELIMITER $$
USE `test`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_user`(
	_email varchar(150),
	_username varchar(50),
	_passwordHash varchar(50),
	_firstName varchar(45),
	_lastName varchar(45)
)
begin

	if exists (select 1 from user where email=_email) then
		select 'This email is in use.' as Error;
	elseif exists (select 1 from user where `name`=_username) then
		select 'This username is in use.' as Error;
	else
		insert user (email, `name`, passwordHash, firstName, lastName, registerDate, lastChangeDate)
		select _email, _username, _passwordHash, _firstName, _lastName, current_timestamp, current_timestamp;

		select last_insert_id() as userID;
	end if;

end$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure create_post
-- -----------------------------------------------------

USE `test`;
DROP procedure IF EXISTS `test`.`create_post`;

DELIMITER $$
USE `test`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `create_post`(
	_userID int,
	_postContent varchar(4096)
)
BEGIN
	insert post (postTypeID, authorID, channelID, replyPostID, `subject`, content)
	select 1, _userID, null, null, null, _postContent;

	select last_insert_id() as postID; 
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure get_post
-- -----------------------------------------------------

USE `test`;
DROP procedure IF EXISTS `test`.`get_post`;

DELIMITER $$
USE `test`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_post`(
	_postID int
)
BEGIN
	select p.id, u.firstName, u.lastName, p.content, p.createDate as postDate
	from post p
		join user u on p.authorID=u.ID
	where p.id = _postID;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure get_posts
-- -----------------------------------------------------

USE `test`;
DROP procedure IF EXISTS `test`.`get_posts`;

DELIMITER $$
USE `test`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_posts`(
	_userID int,
	_sincePostID int,
	_beforePostID int,
	_sinceDate datetime,
	_beforeDate datetime
)
BEGIN

	select *
	from post
	where (_userID is null or post.authorID=_userID)
		and (_sincePostID is null or post.id > _sincePostID)
		and (_beforePostID is null or post.id < _beforePostID)
		and (_sinceDate is null or post.createDate > _sinceDate)
		and (_beforeDate is null or post.createDate < _beforeDate);

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure get_profile
-- -----------------------------------------------------

USE `test`;
DROP procedure IF EXISTS `test`.`get_profile`;

DELIMITER $$
USE `test`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_profile`(
	_userName varchar(50)
)
BEGIN

	select `name`, firstName, lastName 
	from user
	where `name` = _userName;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure get_salt_by_username
-- -----------------------------------------------------

USE `test`;
DROP procedure IF EXISTS `test`.`get_salt_by_username`;

DELIMITER $$
USE `test`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_salt_by_username`(
	in username varchar(50)
)
begin

	select substring(passwordHash, 1, 29) as `salt`
	from user
	where `name`=username;

end$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure user_login
-- -----------------------------------------------------

USE `test`;
DROP procedure IF EXISTS `test`.`user_login`;

DELIMITER $$
USE `test`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `user_login`(username varchar(50), pwordHash varchar(50))
BEGIN
	select *
	from user
	where `name`=username and passwordHash=pwordHash;
END$$

DELIMITER ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
